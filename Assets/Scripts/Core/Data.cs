using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data {
    public const string key_Version = "Version_last";
    public const string key_Audio = "Audio_", key_volumeSound = "volumeSound", key_volumeMusic = "volumeMusic";

    public static float volumeMusic = 0.5f, volumeSound = 0.5f, volumeSfx = 0.5f;

    public static void LoadSettings() {
        // audio
        LoadSetting(key_Audio + key_volumeMusic, ref volumeMusic); // setup music volume
        LoadSetting(key_Audio + key_volumeSound, ref volumeSound); // setup sound volume
    }
    public static void SaveSettings() {
        // General
        SaveSetting(key_Version, Application.version);
        // Audio
        SaveSetting(key_Audio + key_volumeMusic, volumeMusic);
        SaveSetting(key_Audio + key_volumeSound, volumeSound);

        PlayerPrefs.Save();
    }
    static bool LoadSetting(string settingKey, ref float floatToLoad) {
        if(!PlayerPrefs.HasKey(settingKey)) return false;
        floatToLoad = PlayerPrefs.GetFloat(settingKey);
        return true;
    }
    static bool LoadSetting(string settingKey, ref int intToLoad) {
        if(!PlayerPrefs.HasKey(settingKey)) return false;
        intToLoad = PlayerPrefs.GetInt(settingKey);
        return true;
    }
    static bool LoadSetting(string settingKey, ref string strToLoad) {
        if(!PlayerPrefs.HasKey(settingKey)) return false;
        strToLoad = PlayerPrefs.GetString(settingKey);
        return true;
    }
    static void SaveSetting(string settingKey, float floatToSave) {
        PlayerPrefs.SetFloat(settingKey, floatToSave);
    }
    static void SaveSetting(string settingKey, int intToSave) {
        PlayerPrefs.SetInt(settingKey, intToSave);
    }
    static void SaveSetting(string settingKey, string strToSave) {
        PlayerPrefs.SetString(settingKey, strToSave);
    }

    private void OnApplicationQuit() {
        SaveSettings();
    }
}