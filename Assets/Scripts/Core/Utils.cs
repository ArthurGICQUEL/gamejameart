using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils {

    #region MISC

    public static bool LinePlaneIntersection(out Vector3 intersection, Vector3 pointOnPlane, Vector3 planeNormal, Vector3 pointOnLine, Vector3 lineDirection) {
        intersection = pointOnPlane;
        float a, b, x;
        a = Vector3.Dot(pointOnPlane - pointOnLine, planeNormal);
        b = Vector3.Dot(lineDirection, planeNormal);
        bool isParallel = b == 0, isPointOnPlane = a == 0;
        if(isPointOnPlane) {
            intersection = pointOnLine;
            return true;
        } else if(!isParallel) {
            x = a / b;
            intersection = x * lineDirection + pointOnLine;
            return true;
        }
        return false;
    }
    public static void AddUnique<T>(this List<T> list, T element) where T : class {
        if(!list.Contains(element)) list.Add(element);
    }
    public static bool Contains<T>(this T[] array, T element) where T : class {
        if(element == null) return false;
        for(int i=0; i<array.Length; i++) {
            if(array[i] == element) return true;
        }
        return false;
    }
    #endregion

    #region SPECIFICS

    public static float GetValue01(this UnityEngine.UI.Slider slider) {
        float val = Remap(slider.value, slider.minValue, slider.maxValue, 0, 1);
        return val;
        //return (slider.value - slider.minValue) / (slider.maxValue - slider.minValue);
    }
    public static void SetValue01(this UnityEngine.UI.Slider slider, float value01) {
        slider.value = Remap(value01, 0, 1, slider.minValue, slider.maxValue);
    }
    public static float Remap(float inValue, float inMin, float inMax, float outMin, float outMax) {
        return outMin + (inValue - inMin) * (outMax - outMin) / (inMax - inMin);
    }
    public static Vector3 Abs(Vector3 vector) {
        return new Vector3(Mathf.Abs(vector.x), Mathf.Abs(vector.y), Mathf.Abs(vector.z));
    }
    public static void HideCursor(bool hide) {
        //Debug.Log("Show Cursor: " + !hide);
        if(hide) {
            Cursor.lockState = CursorLockMode.Locked;
        } else {
            Cursor.lockState = CursorLockMode.None;
        }
    }
    #endregion

}