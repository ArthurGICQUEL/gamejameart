using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public static MenuManager instance = null;

    [SerializeField] GameObject pauseLayout;
    [SerializeField] Slider sliderMusic, sliderSound;

    private void Awake() {
        if(instance == null) {
            instance = this;
        } else if(instance != this) {
            Destroy(gameObject);
        }
    }

    public void SetupSliders() {
        if(sliderMusic != null) {
            sliderMusic.value = Data.volumeMusic;
        }
        if(sliderSound != null) {
            sliderSound.value = Data.volumeSound;
        }
    }

    public void LoadScene(int i)
    {
        SceneManager.LoadScene(i);
    }
    public void Pause()
    {
        if (GameManager.instance.gamePaused)
        {
            pauseLayout.SetActive(false);
            GameManager.instance.gamePaused = false;
        }
        else
        {
            pauseLayout.SetActive(true);
            GameManager.instance.gamePaused = true;
        }
    }
    public void Quit()
    {
        Application.Quit();
    }
}
