﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NeutralCore.Lang {

    public class TextLang : MonoBehaviour {
        public string keyName; 
        [HideInInspector] public string text;

        Text _textElement = null;
        TextMeshProUGUI _textTMPElement = null;

        protected virtual void Awake() {
            _textElement = GetComponent<Text>();
            _textTMPElement = GetComponent<TextMeshProUGUI>();
        }

        protected virtual void Start() {
            AskForTextUpdate();
        }

        public void AskForTextUpdate() {
            if(_textElement == null && _textTMPElement == null) return;
            LanguageManager.Instance?.RequireTextUpdate(this);
        }

        public void SetText(string text) {
            this.text = text;
        }

        public void UpdateText(string text) {
            if(_textElement != null) {
                _textElement.text = text;
            } else if(_textTMPElement != null) {
                _textTMPElement.text = text;
            }
        }
    }
}