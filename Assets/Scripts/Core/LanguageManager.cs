﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using NeutralCore.Lang;

namespace NeutralCore {

    public class LanguageManager : MonoBehaviour {
        public static LanguageManager Instance = null;

        [Tooltip("Text File should be a .txt containing a header and lines with traductions as follows:\n[LangItemKey name]tab[French translation]tab[English translation]")]
        [SerializeField] TextAsset textFile = null;
        [SerializeField] Lang selectedLanguage = Lang.French;
        [SerializeField] bool canThrowExceptions = true;
        [SerializeField] bool debugMode = true;

        Dictionary<string, LangItem> items = new Dictionary<string, LangItem>();
        List<TextLang> textElements = new List<TextLang>();

        private void Awake() {
            if(Instance == null) Instance = this;
            else if(Instance != this) Destroy(gameObject);
            DontDestroyOnLoad(gameObject);

            ChangeLanguage(selectedLanguage);
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.sceneUnloaded += OnSceneUnloaded;
        }

        private void Start() {
            if(debugMode) DebugTest();
        }

        void DebugTest() {
            // +++++ DEBUG +++++ //
            Debug.Log("Loaded LangItems:");
            //string itemStr;
            foreach(LangItem k in items.Values) {
                //itemStr = "key: " + k + "; text: " + GetText(k) + ";";
                Debug.Log(k.ToString());
            }

            //Debug.Log(GetText(LangItem.Key.name0));
            //Debug.Log(GetText(LangItem.Key.name1));
            //Debug.Log(GetText(LangItem.Key.name2));
            //Debug.Log(GetText(LangItem.Key.name3));
            //Debug.Log(GetText(LangItem.Key.name4));
            //*/
        }

        private void Update() {
            if(debugMode) {
                if(Input.GetKeyDown(KeyCode.E)) {
                    ChangeLanguage(Lang.English);
                } else if(Input.GetKeyDown(KeyCode.F)) {
                    ChangeLanguage(Lang.French);
                }
            }
        }

        void OnSceneLoaded(Scene arg0, LoadSceneMode arg1) {
            //UpdateTexts();
        }

        void OnSceneUnloaded(Scene arg0) {
            textElements.Clear();
        }

        public void RequireTextUpdate(TextLang textLang) {
            AddTextElement(textLang);
            UpdateText(textLang);
        }

        /// <summary>
        /// Load all traductions for this language and apply it to every text.
        /// </summary>
        /// <param name="languageTag">The tag of the language. (e.g. "fr", "fra", "fr-fr", "en", "eng", etc)</param>
        public void ChangeLanguage(string languageTag) {
            languageTag = languageTag.Trim().ToLower();
            Lang lang = selectedLanguage;
            if(languageTag.Equals("fr") || languageTag.Equals("fr-fr") || languageTag.Equals("fra") || languageTag.Equals("french")) {
                lang = Lang.French;
            } else if(languageTag.Equals("en") || languageTag.Equals("en-us") || languageTag.Equals("en-uk") || languageTag.Equals("eng") || languageTag.Equals("english")) {
                lang = Lang.English;
            }
            ChangeLanguage(lang);
        }
        public void ChangeLanguage(Lang lang) {
            selectedLanguage = lang;
            LoadLanguageFile();
            UpdateTexts();
        }

        public string GetText(LangItem.Key key) => GetText(key.ToString());
        public string GetText(string textKey) {
            textKey = textKey.Trim().ToLower();
            if(!items.ContainsKey(textKey)) {
                if(canThrowExceptions) {
                    throw new LanguageException(LanguageException.KeyNotFoundCode, "(key: " + textKey + ")");
                }
                return "[ERROR]";
            }
            try {
                return items[textKey].GetText();
            } catch(LanguageException e) {
                if(canThrowExceptions) {
                    throw e;
                }
                return "[ERROR]";
            }
        }

        void UpdateTexts() {
            // get all text elements found in scene and update them with appropriate trad
            TextLang[] elements = textElements.ToArray();
            Stack<int> toRemove = new Stack<int>();
            for(int i = 0; i < elements.Length; i++) {
                if(elements[i] == null) {
                    toRemove.Push(i);
                    continue;
                }
                UpdateText(elements[i]);
            }
            // clean null objects from textElements list
            for(int i=0; i<toRemove.Count; i++) {
                textElements.RemoveAt(toRemove.Pop());
            }
        }

        void UpdateText(TextLang textLang) {
            textLang.SetText(GetText(textLang.keyName));
        }

        void AddTextElement(TextLang textLang) {
            if(!textElements.Contains(textLang)) {
                textElements.Add(textLang);
            }
        }

        void LoadLanguageFile() {
            string[] lines;
            //string assetText = textFile.text;
            //TextAsset byteAsset = textFile;
            string assetText = System.Text.Encoding.Default.GetString(textFile.bytes);
            lines = Regex.Split(assetText, "\n"); // split text file in lines

            items = new Dictionary<string, LangItem>();
            for(int i = 1; i < lines.Length; i++) { // convert extracted lines into LangItem objects and store them in a dictionary
                //Debug.Log(lines[i]);
                if(lines[i].Trim() == "") continue;
                LangItem item = ConvertLineToItem(lines[i]);
                if(item == null) {
                    if(debugMode) {
                        Debug.LogWarning("LanguageFile line could not be converted to LangItem. Please check file syntax and fix the problem. (at line " + (i + 1) + ")");
                    }
                    continue;
                }
                if(items.ContainsKey(item.name)) {
                    if(debugMode) {
                        Debug.LogWarning($"LanguageFile line uses an existing key and has thus been ignored. Please make sure each key is unique. (key: \"{item.name}\" at line {(i + 1)})");
                    }
                    continue;
                }
                items[item.name] = item;
            }
        }

        LangItem ConvertLineToItem(string line) {
            List<string> elements = new List<string>();
            string temp = "";
            for(int i = 0; i < line.Length; i++) { // separate line with tabulations as separator
                if(line[i] == '\t') {
                    elements.Add(temp);
                    temp = "";
                } else {
                    temp += line[i];
                    if(i == line.Length - 1) {
                        elements.Add(temp);
                    }
                }
            }

            if(elements.Count < 1) return null;
            string name = elements[0].Trim().ToLower();
            //if(name == "") return null;
            elements.RemoveAt(0);
            string[] translations = elements.ToArray();

            return new LangItem(name, translations);
        }

        public class LangItem {
            public string name;
            public string[] translations;

            public LangItem(string name, string[] translations) {
                this.name = name;
                this.translations = translations;
            }

            public string GetText() {
                int langIndex = (int)Instance.selectedLanguage;
                if(translations.Length < langIndex + 1) {
                    throw new LanguageException(LanguageException.TranslationNotFoundCode, "(name: " + name + ")");
                }
                return translations[langIndex];
            }

            public override string ToString() {
                string result = "(name: " + name;
                for(int i = 0; i < translations.Length; i++) {
                    result += "|" + ((Lang)i).ToString() + ": \"" + translations[i] + "\"";
                }
                result += ")";
                return result;
            }

            public enum Key {
                name0, name1, name2, name3
            }
        }

        public class LanguageException : Exception {
            public const int KeyNotFoundCode = 0, TranslationNotFoundCode = 1;

            string _errorMessage;

            public LanguageException(int errorCode, string additionalInfos = "") : base() {
                additionalInfos = additionalInfos.Trim();
                switch(errorCode) {
                    case KeyNotFoundCode:
                        _errorMessage = "The LangItemKey you used doesn't match any text. Please check the LangItemKey used or the language file to find the problem.";
                        break;
                    case TranslationNotFoundCode:
                        _errorMessage = "The translation for this LangItemKey was not found. Please check the selected language or the language file to find the problem.";
                        break;
                    default:
                        _errorMessage = "Undefined LanguageException.";
                        break;
                }
                _errorMessage += (additionalInfos != "" ? " " : "") + additionalInfos;
            }

            public override string Message => _errorMessage;
        }

        public enum Lang {
            French, English
        }
    }
}
