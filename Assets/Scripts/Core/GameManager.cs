using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public bool gamePaused {
        get { return Time.timeScale == 0; }
        set { Time.timeScale = value ? 0 : 1; }
    }
    public Player player;
    public Sheep sheep;
    [SerializeField] Player playerPrefab;
    [SerializeField] float textSpeed = 20;
    [SerializeField] bool isInDialogMode = false;
    public int currentLevel = 1;
    [SerializeField] Vector3[] spawns = new Vector3[3];
    [SerializeField] KeyCode keySkip = KeyCode.Space;

    [SerializeField] Cinematic cinematicLvl3;

    CameraManager cm;
    DialogText nextDialog = null;
    bool firstDialog = true;

    private void Awake()
    {
        instance = this;
        cm = Camera.main.GetComponent<CameraManager>();
        gamePaused = false;
    }
    private void Start() {
        StartDialog(dSci_s1_d1);
        //Debug.Log("current level = " + currentLevel);
    }

    private void Update() {
        //Debug.Log($"gameManager == instance ? => {this == instance}");
        if(Input.GetKeyDown(KeyCode.Escape)) {
            MenuManager.instance.Pause();
        }
        if(gamePaused) return;

        if(!isInDialogMode) {
            if(Input.GetKeyDown(KeyCode.Backspace)) OnRetryClick();
        } else {
            if(Input.GetKeyDown(keySkip)) SkipDialog();
        }
    }

    public void OnRetryClick()
    {
        //play death anim
        Destroy(player.gameObject);
        //Debug.Log(currentlevel - 1);
        player = Instantiate(playerPrefab, spawns[currentLevel - 1], transform.rotation);
        player.FreezeControls(false);
    }

    public void NextLevel() {
        currentLevel += 1;
        //Debug.Log("current level = " + currentLevel);
        cm.ChangeLevel(currentLevel, () => {
            if(GameManager.instance.currentLevel == 2) {
                StartDialog(dSci_s2_d1);
            } else if(GameManager.instance.currentLevel == 3) {
                player.FreezeControls(false);
            }
        });
        player.SetAnimIdle();
        MovePlayerToStartPos();
        sheep.transform.position = player.transform.position + Vector3.right * sheep.minDistanceWithPlayer;
    }

    public void MovePlayerToStartPos() {
        player.transform.position = spawns[currentLevel - 1];
        player.FreezeControls(true);
    }

    public void StartCinematicLvl3(bool start) {
        isInCinematic = true;
        cinematicLvl3.gameObject.SetActive(start);
        cinematicLvl3.LaunchCinematic();
    }

    public void UpdateCinematic(DialogChoice dChoice) {
        Debug.Log($"CurrentChoice: {dChoice}");
        if(dChoice_list_Aim.Contains(dChoice)) {
            cinematicLvl3.Aim();
        } else if(dChoice_list_Shoot.Contains(dChoice)) {
            cinematicLvl3.Shoot();
        } else if(dChoice_list_DropGun.Contains(dChoice)) {
            cinematicLvl3.DropGun();
        }
    }

    // ----------------------- DIALOGS -------------------------- //

    [SerializeField] DialogText dShp_s1_d1, dShp_s1_d2, dShp_s1_d3, dShp_s2_d1;
    [SerializeField] DialogText dSci_s1_d1, dSci_s2_d1, dSci_s3_d1, dSci_s3_d2, dSci_s3_d3, dSci_s3_d4;
    [SerializeField] DialogText[] dSci_s3_rerolls = null;
    [SerializeField] DialogChoice[] dChoice_list_Aim, dChoice_list_DropGun, dChoice_list_Shoot;
    [SerializeField] Dialog dialBot, dialSci, dialSheep, dialQ1, dialQ2, dialQ3;
    Dialog currentDialog;
    DialogChoice currentChoice;
    public bool isInCinematic = false;

    void SkipDialog() {
        currentDialog?.Skip();
    }

    public void StartDialog(DialogText dObj) {
        GoInDialogMode();
        currentDialog = GetDialRef(dObj);
        /*
        Debug.Log($"CurrentChoice: {currentChoice}");
        if(dChoice_list_Aim.Contains(currentChoice)) {
            cinematicLvl3.Aim();
        } else if(dChoice_list_Shoot.Contains(currentChoice)) {
            cinematicLvl3.Shoot();
        } else if(dChoice_list_DropGun.Contains(currentChoice)) {
            cinematicLvl3.DropGun();
        }
        currentChoice = null;//*/
        ActivateDialBubble(currentDialog, true);
        currentDialog.Display(dObj, textSpeed);
    }
    public void StartDialog(DialogChoice dObj, int i) {
        GoInDialogMode();
        currentDialog = null;
        currentChoice = dObj;
        Dialog dial = GetDialRef(dObj, i);
        ActivateDialBubble(dial, true);
        dial.DisplayInstant(dObj);
    }
    public void GoInDialogMode() {
        isInDialogMode = true;
        // freeze player controls
        player.FreezeControls(true);
    }
    public void StartDialogs(DialogObject[] dObjs) {
        DialogText dTxt;
        DialogChoice dChoice;
        for(int i=0; i<dObjs.Length; i++) {
            dTxt = dObjs[i] as DialogText;
            dChoice = dObjs[i] as DialogChoice;
            if(dTxt != null) StartDialog(dTxt);
            if(dChoice != null) StartDialog(dChoice, i);
        }
    }

    public void ExitDialogMode() {
        HideChoiceButtons();
        ActivateDialBubble(dialBot, false);
        ActivateDialBubble(dialSci, false);
        ActivateDialBubble(dialSheep, false);
        if(!dChoice_list_DropGun.Contains(currentChoice)) {
            isInDialogMode = false;
            player.FreezeControls(false);
            if(firstDialog) {
                firstDialog = false;
                StartDialog(dShp_s1_d1);
            }
        }
    }

    public void HideChoiceButtons() {
        ActivateDialBubble(dialQ1, false);
        ActivateDialBubble(dialQ2, false);
        ActivateDialBubble(dialQ3, false);
    }

    void ActivateDialBubble(Dialog dial, bool activate) {
        dial?.transform.parent.gameObject.SetActive(activate);
    }

    Dialog GetDialRef(DialogText dObj) {
        switch(dObj.dialogBubble) {
            case DialogBubble.Robot:
                return dialBot;
            case DialogBubble.Scientist:
                return dialSci;
            case DialogBubble.Sheep:
                return isInCinematic ? dialBot : dialSheep;
            default:
                return null;
        }
    }
    Dialog GetDialRef(DialogChoice dObj, int i) {
        switch(i) {
            case 0:
                return dialQ1;
            case 1:
                return dialQ2;
            case 2:
                return dialQ3;
            default:
                return null;
        }
    }

    private void OnDestroy() {
        if(instance == this) instance = null;
    }
}