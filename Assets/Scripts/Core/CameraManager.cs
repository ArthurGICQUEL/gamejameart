using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public float lerpTime;
    Camera cam;
    Vector3 initialPosition;
    public bool isLerping = false;

    private void Awake()
    {
        cam = GetComponent<Camera>();
        initialPosition = transform.position;
    }
    private void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.I)) ChangeLevel(1);
        if (Input.GetKeyDown(KeyCode.O)) ChangeLevel(2);
        if (Input.GetKeyDown(KeyCode.P)) ChangeLevel(3);
        //*/
    }
    public void ChangeLevel(int level, Action action = null)
    {
        //transform.position = initialPosition + new Vector3(camera.aspect * camera.orthographicSize * 2 * (level - 1), 0, 0);
        //if (!isLerping) StartCoroutine(LerpCamera(initialPosition + new Vector3(cam.aspect * cam.orthographicSize * 2 * (level - 1) + 2 * (level - 1), 0, 0), 3, action));
        if(!isLerping) StartCoroutine(LerpCamera(initialPosition + Vector3.right * 5 * (level - 1), lerpTime, action));
    }
    public IEnumerator LerpCamera(Vector3 targetPosition, float timeToLerp, Action action = null)
    {
        isLerping = true;
        Vector3 startPosition = transform.position;
        for (float t = 0; t < timeToLerp; t += Time.deltaTime)
        {
            //Debug.Log("lala");
            transform.position = Vector3.Lerp(startPosition, targetPosition, t / timeToLerp);
            yield return null;
        }
        transform.position = targetPosition;
        isLerping = false;
        action.Invoke();
    }
}
