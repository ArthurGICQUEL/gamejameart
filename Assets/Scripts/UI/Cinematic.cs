using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cinematic : MonoBehaviour {
    public Image blackScreen, background;
    public Image botGunDownPickedUp, botGunAimed, botGunAimedDropped, botGunAimedSmoking;
    public Image sheepNormal, sheepSad, sheepDead;
    public Image gunPos1, gunPos2, gunPos3, gunOnGround;

    public float delayBetweenFrames = 1;

    public void LaunchCinematic() {
        //GameManager.instance.player.FreezeControls(true);
        SetInitial();
    }

    public void ExitCinematic() {
        gameObject.SetActive(false);
        GameManager.instance.player.FreezeControls(false);
        GameManager.instance.isInCinematic = false;
    }

    public void SetInitial() {
        background.gameObject.SetActive(true);
        blackScreen.gameObject.SetActive(false);

        botGunDownPickedUp.gameObject.SetActive(true);
        botGunAimed.gameObject.SetActive(false);
        botGunAimedDropped.gameObject.SetActive(false);
        botGunAimedSmoking.gameObject.SetActive(false);

        sheepNormal.gameObject.SetActive(true);
        sheepSad.gameObject.SetActive(false);
        sheepDead.gameObject.SetActive(false);

        SetGunPos(0);
    }

    public void Aim() {
        Debug.Log("Aim !");
        botGunDownPickedUp.gameObject.SetActive(false);
        botGunAimed.gameObject.SetActive(true);
        botGunAimedDropped.gameObject.SetActive(false);
        botGunAimedSmoking.gameObject.SetActive(false);

        sheepNormal.gameObject.SetActive(false);
        sheepSad.gameObject.SetActive(true);
        sheepDead.gameObject.SetActive(false);

        SetGunPos(0);
    }

    public void BotHasShot() {
        Debug.Log("BotHasShot !");
        botGunDownPickedUp.gameObject.SetActive(false);
        botGunAimed.gameObject.SetActive(false);
        botGunAimedDropped.gameObject.SetActive(false);
        botGunAimedSmoking.gameObject.SetActive(true);
    }

    public void SheepIsDead() {
        Debug.Log("SheepIsDead !");
        sheepNormal.gameObject.SetActive(false);
        sheepSad.gameObject.SetActive(false);
        sheepDead.gameObject.SetActive(true);
    }

    public void Shoot() {
        StartCoroutine(Shooting());

    }

    IEnumerator Shooting() {
        Debug.Log("Shooting 0/4 (start of process)");
        Aim();

        Debug.Log($"Shooting 1/4 (black screen)");
        blackScreen.gameObject.SetActive(true);
        //yield return new WaitForSeconds(delayBetweenFrames);
        Debug.Log("Shooting 2/4 (gunfire sound)");
        // shooting sound
        // wait for sound to finish
        yield return new WaitForSeconds(delayBetweenFrames);
        Debug.Log("Shooting 3/4 (black screen off)");
        blackScreen.gameObject.SetActive(false);

        BotHasShot();
        SheepIsDead();
        yield return new WaitForSeconds(delayBetweenFrames * 2);
        Debug.Log("Shooting 4/4 (end of process)");
        ExitCinematic();
    }

    public void DropGun() {
        Debug.Log("DropGun !");
        botGunDownPickedUp.gameObject.SetActive(false);
        botGunAimed.gameObject.SetActive(false);
        botGunAimedDropped.gameObject.SetActive(true);

        sheepNormal.gameObject.SetActive(true);
        sheepSad.gameObject.SetActive(false);
        sheepDead.gameObject.SetActive(false);

        SetGunPos(0);
        StartCoroutine(DroppingGun(delayBetweenFrames));
    }

    void SetGunPos(int posIndex) {
        gunPos1.gameObject.SetActive(posIndex == 1);
        gunPos2.gameObject.SetActive(posIndex == 2);
        gunPos3.gameObject.SetActive(posIndex == 3);
        gunOnGround.gameObject.SetActive(posIndex == 4);
    }

    IEnumerator DroppingGun(float delayBetweenFrames) {
        for(int i=1; i<5; i++) {
            SetGunPos(i);
            yield return new WaitForSeconds(delayBetweenFrames);
        }
        ExitCinematic();
    }

    IEnumerator PlayDelay(float delayBetweenFrames) {
        for(float t= 0; t < delayBetweenFrames; t += Time.deltaTime) {
            yield return null;
        }
    }
}