using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Activable {
    bool locked = true, used = false;

    protected override void OnActivate() {
        locked = false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!used && !locked) {
            used = true;
            GameManager.instance.NextLevel();
        }
    }
}
