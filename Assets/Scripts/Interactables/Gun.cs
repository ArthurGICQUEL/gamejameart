using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : Interactable {
    [SerializeField] DialogText dialogFirst, dialogThen;

    protected override void OnActivate() {
        Debug.Log("Gun activated!");
        // go in cinematic mode
        GameManager.instance.StartCinematicLvl3(true);
        // start dialog
        if(timesUsed < 1 && dialogFirst != null) {
            GameManager.instance.StartDialog(dialogFirst);
        } else if(timesUsed > 0 && dialogThen != null) {
            GameManager.instance.StartDialog(dialogThen);
        }
    }
}