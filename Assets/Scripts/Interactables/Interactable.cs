using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : Activable {
    protected int timesUsed = 0;

    bool isReachable = false;
    [SerializeField] protected int maxUse = 1;

    protected virtual void Update() {
        if((maxUse < 0 || timesUsed < maxUse) && isReachable
            && !GameManager.instance.gamePaused && !GameManager.instance.player.isFrozen && Input.GetKeyDown(KeyCode.E)) {
            Activate();
            timesUsed++;
            //Debug.Log($"{name} activated. (used:{timesUsed} / max:{maxUse})");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        //Debug.Log($"{collision.name} (tag: {collision.tag}) collided with {name}");
        if(collision.CompareTag("Player")) {
            isReachable = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if(collision.CompareTag("Player")) {
            isReachable = false;
        }
    }
}