using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activable : MonoBehaviour {
    protected Animator anim;

    protected virtual void Awake() {
        anim = GetComponent<Animator>();
    }

    public void Activate() {
        if(anim != null) anim.SetTrigger("Activate");
        OnActivate();
    }

    protected virtual void OnActivate() {
        // do nothing
    }
}