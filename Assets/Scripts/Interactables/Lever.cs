using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : Interactable {
    [SerializeField] Activable[] targets;
    [SerializeField] DialogText dialogToLaunch;
    [SerializeField] float dialogDelay;

    protected override void OnActivate() {
        GetComponent<BoxCollider2D>().enabled = false;
        GameManager.instance.player.PlayAnimInteract();
        if(dialogToLaunch != null) {
            Invoke(nameof(LaunchDialog), dialogDelay);
        }

        foreach(Activable target in targets) {
            target.Activate();
        }
    }

    void LaunchDialog() {
        GameManager.instance.StartDialog(dialogToLaunch);
    }
}

