using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TubeGun : Activable {
    [SerializeField] Gun prefabGun;
    protected override void OnActivate() {
        Instantiate(prefabGun, transform.position, prefabGun.transform.rotation, transform.parent);
    }
}