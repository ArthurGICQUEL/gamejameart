using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : Activable {
    protected override void OnActivate() {
        GetComponent<BoxCollider2D>().enabled = false;
        // or play fake anim for falling box
    }
}