using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sheep : MonoBehaviour
{
    Player player;
    public float minDistanceWithPlayer = .35f, speed = 1, distanceBeforeJump = .5f, distanceBeforeHole = .3f, holeDeepness = .25f, jumpForce = 3;
    Rigidbody2D rb;
    Vector2 movement = Vector2.zero;
    Vector3 originalScale;
    int scaleMultiplier = 1;
    Animator anim;
    Transform tBubble;

    bool isGrounded
    {
        set
        {
            _isGrounded = value;
            //GetComponent<SpriteRenderer>().color = value ? Color.magenta : Color.cyan;
        }
        get
        {
            return _isGrounded;
        }
    }
    bool _isGrounded;
    bool canJump;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        originalScale = transform.localScale;
        anim = GetComponent<Animator>();
        tBubble = transform.GetChild(0);
    }
    private void Update()
    {
        if(player == null) player = GameManager.instance.player;
        SetupFlip();

        if(!player.isFrozen) {
            if(Mathf.Abs(player.transform.position.x - transform.position.x) > minDistanceWithPlayer) {
                movement.x = (transform.position.x < player.transform.position.x ? 1 : -1) * speed;
            } else {
                movement.x = 0;
            }
        } else {
            movement.x = 0;
        }

        if (!isGrounded) movement.y += Physics2D.gravity.y * Time.deltaTime;
        else if (rb.velocity.y < 0) movement.y = 0;

        if(!player.isFrozen) {
            if(isGrounded
                && (Physics2D.Raycast(transform.position, new Vector2(movement.x, 0), distanceBeforeJump, 1 << LayerMask.NameToLayer("Ground"))
                    || !Physics2D.Raycast(transform.position + Vector3.right * movement.normalized.x * distanceBeforeHole, Vector3.down, holeDeepness, 1 << LayerMask.NameToLayer("Ground")))) {
                anim.SetTrigger("jump");
                movement.y = jumpForce;
            }
        }
        rb.velocity = movement;


        //anim
        anim.SetFloat("velocity", Mathf.Abs(movement.x));
    }

    public void SetupFlip() {
        if(player.transform.position.x > transform.position.x) scaleMultiplier = 1;
        else scaleMultiplier = -1;
        transform.localScale = new Vector3(originalScale.x * scaleMultiplier, originalScale.y, originalScale.z);
        tBubble.localScale = new Vector3(Mathf.Abs(tBubble.localScale.x) * scaleMultiplier, tBubble.localScale.y, tBubble.localScale.z);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ground") 
        {
            isGrounded = true;
            anim.SetBool("isGrounded", true);
        } 
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            isGrounded = false;
            anim.SetBool("isGrounded", false);
        }
    }
}
