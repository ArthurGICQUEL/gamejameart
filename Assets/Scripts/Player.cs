using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    CameraManager cm;
    Rigidbody2D rb;
    bool isGrounded {
        get { return groundPoints > 0; }
    }
    int groundPoints = 0;
    [SerializeField] float speed = 3, jumpForce = 2;
    Vector2 movement = Vector2.zero;
    SpriteRenderer sr;
    Vector3 originalScale;
    int scaleMultiplier = 1;
    Animator anim;
    public bool isFrozen = true;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        cm = Camera.main.GetComponent<CameraManager>();
        sr = GetComponent<SpriteRenderer>();
        originalScale = transform.localScale;
        anim = GetComponent<Animator>();
    }
    void Update()
    {
        if(isFrozen) return;

        movement.x = Input.GetAxis("Horizontal")*speed;

        if (movement.x > 0) scaleMultiplier = 1;
        else if (movement.x < 0) scaleMultiplier = -1;
        transform.localScale = new Vector3(originalScale.x * scaleMultiplier, originalScale.y, originalScale.z);

        if (!isGrounded) 
        { 
            movement.y += Physics2D.gravity.y * Time.deltaTime;
            //Debug.Log("move" + movement.y);
            //Debug.Log("rb" + rb.velocity.y);
        } 
        else if (rb.velocity.y < 0) movement.y = 0;
        if (isGrounded && Input.GetKeyDown(KeyCode.Z))
        {
            anim.SetTrigger("jump");
            movement.y = jumpForce;
        }

        rb.velocity = movement;

        //anim
        anim.SetFloat("velocity", Mathf.Abs(movement.x));
    }

    public void PlayAnimInteract() {
        anim.SetTrigger("interact");
    }

    public void SetAnimIdle() {
        anim.SetBool("isGrounded", true);
        anim.SetFloat("velocity", 0);
    }

    public void FreezeControls(bool freeze) {
        isFrozen = freeze;
        if(isFrozen) rb.velocity = Vector2.zero;
    }
    public void SetVelocity(Vector2 velocity) {
        rb.velocity = velocity;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            groundPoints++;
            anim.SetBool("isGrounded", isGrounded);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            groundPoints--;
            anim.SetBool("isGrounded", isGrounded);
        }
    }
}
