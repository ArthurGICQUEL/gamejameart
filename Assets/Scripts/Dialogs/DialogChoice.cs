using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New DialogChoice", menuName = "ScriptableObjects/DialogChoice", order = 3)]
public class DialogChoice : DialogObject {
    public DialogText nextDialog;

    public override string ToString() {
        return $"[DialogChoice (key: {dialogKey})]";
    }
}
