using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New DialogText", menuName = "ScriptableObjects/DialogText", order = 2)]
public class DialogText : DialogObject {
    public DialogBubble dialogBubble;
    public DialogObject[] nextDialogs;
}
