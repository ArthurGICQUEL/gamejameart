using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NeutralCore.Lang;

[RequireComponent(typeof(TextLang))]
public class Dialog : MonoBehaviour {
    [SerializeField] bool activeAtAwake = true;
    TextLang txtLang;
    DialogObject dObj;
    Coroutine slowDisplay = null;

    private void Awake() {
        txtLang = GetComponent<TextLang>();
        if(!activeAtAwake) transform.parent.gameObject.SetActive(false);
    }

    public void Display(DialogObject dObj, float txtSpeed) {
        this.dObj = dObj;
        txtLang.keyName = dObj.dialogKey;
        txtLang.AskForTextUpdate();
        txtLang.UpdateText("");

        slowDisplay = StartCoroutine(DisplaySlow(txtSpeed));
    }
    public void DisplayInstant(DialogObject dObj) {
        this.dObj = dObj;
        txtLang.keyName = dObj.dialogKey;
        txtLang.AskForTextUpdate();
        txtLang.UpdateText(txtLang.text);
        slowDisplay = null;
    }
    IEnumerator DisplaySlow(float speed) {
        string txtToDisplay = txtLang.text;
        //string displayedTxt = "";
        float t;
        for(int i = 0; i < txtToDisplay.Length; i++) {
            for(t=0; t<1f/speed; t += Time.deltaTime) {
                yield return null;
            }
            //Debug.Log(txtToDisplay[i]);
            txtLang.UpdateText(txtToDisplay.Substring(0, i + 1));
        }
        slowDisplay = null;
    }

    public void Skip() {
        if(slowDisplay != null) {
            StopCoroutine(slowDisplay);
            txtLang.UpdateText(txtLang.text);
            slowDisplay = null;
        } else {
            CallNextDialog();
        }
    }

    public void CallNextDialog() {
        GameManager.instance.HideChoiceButtons();
        DialogText dTxt = dObj as DialogText;
        DialogChoice dChoice = dObj as DialogChoice;
        if(dChoice != null) {
            GameManager.instance.UpdateCinematic(dChoice);
            if(CheckIfEndDialog(dChoice)) {
                GameManager.instance.ExitDialogMode();
                return;
            }
            GameManager.instance.StartDialog(dChoice.nextDialog);
        } else if(dTxt != null) {
            if(CheckIfEndDialog(dTxt)) {
                GameManager.instance.ExitDialogMode();
                return;
            }
            GameManager.instance.StartDialogs(dTxt.nextDialogs);
        }
    }

    bool CheckIfEndDialog(DialogText dTxt) {
        return dTxt.nextDialogs.Length == 0;
    }

    bool CheckIfEndDialog(DialogChoice dChoice) {
        return dChoice.nextDialog == null;
    }
}