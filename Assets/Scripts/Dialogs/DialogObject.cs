using System.Collections;
using System.Collections.Generic;
using NeutralCore.Audio;
using UnityEngine;

public class DialogObject : ScriptableObject {
    public string dialogKey;
}
public enum DialogBubble {
    None, Robot, Scientist, Sheep
}